#!/bin/sh
aws s3 rm s3://example-application-uploader --recursive
aws s3 rm s3://example-thumbnail-generator-files --recursive
cd terraform
terraform init
terraform destroy -auto-approve
cd ..
cd terraform-infra
terraform destroy -auto-approve