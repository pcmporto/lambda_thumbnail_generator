# Lambda thumbnail generator #

This project is an example / tutorial on how to create a lambda function on AWS to generate thumbnails.

To better understand the content of this article, you need to know the basics about terraform, which is AWS (Amazon Web Services) and NodeJS.

You read my article [here](https://dev.to/cesarpaulomp/gerando-thumbnails-utilizando-nodejs-ffmpeg-e-aws-lambda-terraform-39p6-temp-slug-9720694?preview=f7888372fa051f5d665ca91783a5bf4db6f239b1660f59fee3a4e2ef1d1793278f63effce0a27b4fa2b284a7913d20875091ee0de387ae8fb13a5dc2). It is in Portuguese Brazil.

### Tools ###
* [AWS Lambda](https://aws.amazon.com/en/lambda/)
* [AWS SQS (Simple Queue Service)](https://aws.amazon.com/pt/sqs/)
* [AWS S3](https://aws.amazon.com/s/s3/)
* [FFMpeg](https://www.ffmpeg.org/)
* [Nodejs](https://nodejs.org/en/)
* [Terraform](https://www.terraform.io/)

### Deploy ###

To avoid problems, I ask you to change the name of the buckets throughout the project. Unfortunately the bucket name is unique to everyone.
Also remember to change YOURACCOUNTID tags

#### Creating bucket for uploading the code ####

* cd terraform-infra
* terraform apply -auto-approve

#### Installing dependencies, generating a zip file and upload our code to s3 ####

* npm install --production
* zip lambda.zip -r node_modules main package.json index.js
* aws s3 cp lambda.zip s3://example-application-uploader/thumbnail/lambda.zip

#### Creating our lambda function and all the necessary infrastructure ####

* cd terraform
* terraform init
* terraform apply -auto-approve

#### destroying the application (undeploy) ####

* aws s3 rm s3://example-application-uploader --recursive
* aws s3 rm s3://example-thumbnail-generator-files --recursive
* cd terraform
* terraform init
* terraform destroy -auto-approve
* cd ..
* cd terraform-infra
* terraform destroy -auto-approve

### Performing unit tests ###

* npm install
* npm run test