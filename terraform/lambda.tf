# Cria grupo de log no cloudwatch.
# Infelizmente é a melhor forma de debugar o lambda
# e tbm é o logger mais fácil de ser plugado no serviço.
resource "aws_cloudwatch_log_group" "thumbnail_generator_lambda_log_group" {
  name              = aws_lambda_function.example-thumbnail-generator-lambda.function_name
  retention_in_days = 1
}

#Criamos aqui a role com as permições básicas para execução do serviço
resource "aws_iam_role" "thumbnail_generator_lambda_iam_role" {
  name = "thumbnail_generator_lambda_iam_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#aqui criamos uma política definindo quais são os recursos da aws que o lambda 
#pode acessar.
#Estamos o autorizando a escrever, enviar e apagar mensagens nas filas,
#a ler, listar, salvar e editar arquivos no bucket e escrever os
#logs no cloudwatch.
resource "aws_iam_role_policy" "thumbnail_generator_lambda_iam_policy" {
  name = "thumbnail_generator_lambda_iam_policy"
  role = aws_iam_role.thumbnail_generator_lambda_iam_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [ 
        "sqs:SendMessage",
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes",
        "sqs:ChangeMessageVisibility"
      ],
      "Resource": [
        "arn:aws:sqs:us-east-1:YOURACCOUNTID:thumbnail-request-queue",
        "arn:aws:sqs:us-east-1:YOURACCOUNTID:thumbnail-request-queue/*",
        "arn:aws:sqs:us-east-1:YOURACCOUNTID:thumbnail-result-queue",
        "arn:aws:sqs:us-east-1:YOURACCOUNTID:thumbnail-result-queue/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "sqs:ListQueues"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:CreateLogGroup",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketLocation",
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:GetObject",
        "s3:GetObjectAcl"
      ],
      "Resource": [
        "arn:aws:s3:::example-thumbnail-generator-files",
        "arn:aws:s3:::example-thumbnail-generator-files/*"
      ]
    }
  ]
}
EOF
}

resource "aws_lambda_function" "example-thumbnail-generator-lambda" {
  s3_bucket        = "example-application-uploader"
  s3_key           = "thumbnail/lambda.zip"
  
  #Uma alternativa ao S3 é utilizar o filebase64sha256
  #recomendo apenas projetos onde o zip fique pequeno.
  #filename         = "lambda.zip"
  #source_code_hash = filebase64sha256("lambda.zip")
  
  function_name    = "example_thumbnail_generator_lambda"
  role             = aws_iam_role.thumbnail_generator_lambda_iam_role.arn
  handler          = "index.handler"
  runtime          = "nodejs10.x"
  memory_size      = 128 // Free Tier
  timeout          = 60
  publish          = true

  environment {
    variables = {
      RESULT_QUEUE_URL  = "https://sqs.us-east-1.amazonaws.com/YOURACCOUNTID/thumbnail-result-queue",
      BUCKET            = "example-thumbnail-generator-files",
      REGION            = "us-east-1"
    }
  }
}

#Este trecho cria o gatilho do nosso lambda. No caso é a nossa fila thumbnail-request-queue.
#Basicamente sempre que chegar uma mensagem a aws dispara nosso lambda
resource "aws_lambda_event_source_mapping" "thumbnail_generator_lambda_source_mapping" {
  event_source_arn = "arn:aws:sqs:us-east-1:YOURACCOUNTID:thumbnail-request-queue"
  enabled          = true
  function_name    = aws_lambda_function.example-thumbnail-generator-lambda.arn
  #Maior número de registros que o lambda pode receber por execução
  batch_size       = 1
}