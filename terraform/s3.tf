resource "aws_s3_bucket" "thumbnails-s3-bucket" {
  bucket = "example-thumbnail-generator-files"
  acl    = "private"

  tags = {
    Team       = "Thumbnail"
    Terraform = "TRUE"
  }
}