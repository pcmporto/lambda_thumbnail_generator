resource "aws_sqs_queue" "thumbnail_request_queue" {
  name = "thumbnail-request-queue"
  visibility_timeout_seconds = 300
  tags = {
    Team = "Thumbnail",
    Terraform = "TRUE"
  }
}

resource "aws_sqs_queue" "thumbnail_result_queue" {
  name = "thumbnail-result-queue"
  visibility_timeout_seconds = 300
  tags = {
    Team = "Thumbnail",
    Terraform = "TRUE"
  }
}