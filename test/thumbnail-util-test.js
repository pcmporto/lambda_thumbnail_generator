const expect = require("chai").expect

const thumbnail = require('../main/utils/thumbnail-util')
const fs = require('fs')

describe('thumbnail-util-test', () => {
    it('generate thumbnail', (done) => {
        let options = {
            source: 'test/test.mp4',
            output: '/tmp/test.jpeg',
            startTime: 0,
            onError: (error) => {
                console.log(error)
                done('expected success')
            },
            onEnd: () => {
                let fileStatus = fs.statSync('/tmp/test.jpeg')
                console.log(fileStatus)
                expect(fileStatus).is.not.undefined
                expect(fileStatus.size).is.not.undefined
                done()
            }
        }

        thumbnail.exec(options)
    })

    it('contentType', () => {
        expect(thumbnail.contentType()).to.be.eq('image/jpg')
    })
})
