const expect = require("chai").expect

const index = require('../index')

describe('index-test', () => {
    it('messageParser', () => {
        let message = index.messageParser({
            Records: [
                {
                    body: `{
                        "source": "https://s3.amazonaws.com/player.buildstaging.com/transcoder-tests/thumbnail_erro_139.mp4",
                        "path": "video/YX8/thumbnail",
                        "startTime": 15
                    }`
                }
            ]
        })
        console.log(message)
        expect(message).to.be.deep.eq({
            source: "https://s3.amazonaws.com/player.buildstaging.com/transcoder-tests/thumbnail_erro_139.mp4",
            path: "video/YX8/thumbnail",
            startTime: 15
        })
    })
})