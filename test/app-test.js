const expect = require("chai").expect
const sinon = require("sinon")

let sqsUtil = require('../main/utils/sqs-util')
let s3Util = require('../main/utils/s3-util')
let thumbnailUtil = require('../main/utils/thumbnail-util')

let App = require('../main/app')

describe('app-test', () => {
    before(() => {
        sinon.stub(sqsUtil, 'sendMessage').returns({})
        sinon.stub(s3Util, 'upload').returns({})
        sinon.stub(thumbnailUtil, 'exec').returns({})
    })

    after(() => {
        sinon.restore()
    })

    it('run', () => {
        let app = new App('https://someurl/video.mp4', 'video/XPTO', 1)
        app.run()
    })
})