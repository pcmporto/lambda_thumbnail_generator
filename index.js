const App = require('./main/app')

/* Função para validar o corpo da mensagem.
    {
        Records: [
            {
                body: "{raw json message}"
            }
        ]
    }
 */
let messageParser = (event) => {
    //Records[] sempre tem um item no array
    let strbody = event.Records[0].body
    try {
        let message = JSON.parse(strbody)

        if (!message.hasOwnProperty('source') ||
            !message.hasOwnProperty('path') ||
            !message.hasOwnProperty('startTime')) {
                console.log('unparseable sqs message')
                console.log(message)
        } else {
            return message;
        }
    } catch (error) {
        console.log('unparseable sqs message')
        console.log(strbody)
    }   

}

//este é o método a ser executado inicialmente pelo lambda
exports.handler = (event, context) => {
    
    let message = messageParser(event)

    if (message) {
        let app = new App(
            //source será a url do vídeo
            message.source,
            //path é o diretório que salvaremos o arquivo gerado
            message.path,
            //Qual segundo do vídeo queremos retirar a imagem
            message.startTime)
        
        app.run()
    }

}

//expõem o método messageParser apenas para teste unitário
exports.messageParser = messageParser;