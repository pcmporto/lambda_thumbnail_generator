#!/bin/sh

cd terraform-infra

terraform init
terraform apply -auto-approve

cd ..

npm install --production
zip lambda.zip -r node_modules main package.json index.js

#outras versões precisam ter o nome do arquivo diferente.
#se nao alterar ao realizar o deploy o terraform entende que
#não ouve alteração no serviço.x
aws s3 cp lambda.zip s3://example-application-uploader/thumbnail/lambda.zip

cd terraform

terraform init
terraform apply -auto-approve