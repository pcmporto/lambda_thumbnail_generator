resource "aws_s3_bucket" "application-uploader-files-bucket" {
  bucket = "example-application-uploader"
  acl    = "private"

  tags = {
    Team      = "Devops"
    Terraform = "TRUE"
  }
}