const thumbnail = require('./utils/thumbnail-util')
const s3util = require('./utils/s3-util')
const sqsUtil = require('./utils/sqs-util')

class App {
    constructor (source, path, startTime) {
        this.fileName = 'thumbnail.jpeg'
        this.output = `/tmp/${this.fileName}`
        this.bucketFileKey = `${path}/${this.fileName}`
        this.path = path
        this.source = source
        this.startTime = startTime
    }

    async run() {
        try {
            await this.generateThumbnail()
            await this.uploadThumbnail()
            await this.notifyDone()
        } catch (e) {
            console.log('Unexpected error')
            console.log(e)
            await this.notifyError()
        }
    }

    generateThumbnail () {
        console.log("generating thumbnail STARTED")
        return new Promise ( (res, rej) => {
            thumbnail.exec({
                source: this.source,
                output: this.output,
                startTime: this.startTime,
                onError: (err) => {
                    console.log(`generating thumbnail FINISHED WITH ERROR: ${err}`)
                    rej(err)
                },
                onEnd: () => {                    
                    console.log(`generating thumbnail FINISHED`)
                    res()
                }
            })
        })
    }

    uploadThumbnail () {
        console.log('Uploadging thumbnail to S3')
        return s3util.upload(
            this.bucketFileKey,
            this.output,
            thumbnail.contentType())
    }


    notifyError() {
        let body = {
            source : this.source,
            startTime : this.startTime,
            key : this.bucketFileKey,
            path : this.path,
            success: false
        }
        console.log('Sending error message to Sqs')
        return sqsUtil.sendMessage(body, 0)
    }

    notifyDone() {
        let body = {
            source : this.source,
            startTime : this.startTime,
            key : this.bucketFileKey,
            path : this.path,
            success: true
        }
        console.log('Sending success message to Sqs')
        return sqsUtil.sendMessage(body, 0)
    }
}

module.exports = App