const AWS = require('aws-sdk')
const fs = require('fs')

//Não precisamos de nenhuma configuração adicional no client
//As credenciais já estão associadas a instancia no lambda
let s3 = new AWS.S3()

//Criamos uma classe com a responsabilidade de subir nosso arquivo no bucket
class S3Util {
    upload(key, orign, contentType) {
        return s3.upload({
            Bucket: process.env.BUCKET,
            Key: key,
            Body: fs.createReadStream(orign),
            ACL: 'private',
            ContentType: contentType,
            StorageClass: 'STANDARD_IA'
        }).promise()
    }
}

module.exports = new S3Util()