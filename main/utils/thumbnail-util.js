const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path
var FFmpeg = require('fluent-ffmpeg')
// set ffmpeg package path
FFmpeg.setFfmpegPath(ffmpegPath)

class ThumbnailGenerator {
    contentType () {
        return 'image/jpg'
    }

    exec (options) {
        new FFmpeg({ source: options.source })
            .withNoAudio()
            .setStartTime(options.startTime)
            .takeFrames(1)
            .withVideoCodec('mjpeg')
            .saveToFile(options.output)
            .on('start', (commandLine) => {
                console.log(`command-line: ${commandLine}`)
            })
            .on('error', (err) => {
                console.log('Error generating thumbnail:')
                console.log(err)
                
                if (options.onError) {
                    options.onError(err)
                }
            })
            .on('end', () => {
                if (options.onEnd) {
                    options.onEnd()
                }
            })
    }
}

module.exports = new ThumbnailGenerator()