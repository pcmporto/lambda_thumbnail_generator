const AWS = require('aws-sdk')

class SqsUtil {
    constructor() {
        this.sqs = new AWS.SQS({region: process.env.REGION})
    }

    sendMessage (body, delay) {
        var sqsMessage = {
            DelaySeconds: delay ? delay : 10,
            MessageBody: JSON.stringify(body),
            QueueUrl: process.env.RESULT_QUEUE_URL
        };

        return new Promise( (res, rej) => {
            this.sqs.sendMessage(sqsMessage, (err, data) => {
                if (err) {
                    rej(err)
                } else {
                    res(data.MessageId)
                }
            })
        })
    }
}

module.exports = new SqsUtil()